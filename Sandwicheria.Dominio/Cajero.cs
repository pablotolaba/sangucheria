﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sandwicheria.Dominio
{
    public class Cajero
    {
        public String _nombre { get; set; }
        public int _codigo { get; set; }
        public Usuario _usuario { get; set; }
        public List<Pedido> _listaPedidos = new List<Pedido>();
        public List<Pedido> Pedidos => _listaPedidos;

        public Cajero(Usuario usuario)
        {
            _usuario = usuario;
        }
        public void AgregarPedido(Pedido pedido)
        {
            _listaPedidos.Add(pedido);
        }
     
    }
}
