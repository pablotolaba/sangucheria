﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sandwicheria.Dominio
{
    public enum Tipo
    {
        Sanguche,
        Bebida,
        Guarnicion
    }
}
