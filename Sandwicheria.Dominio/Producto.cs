﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sandwicheria.Dominio
{
    public abstract class Producto
    {
        private int _codigo;
        private String _descripcion;
        private double _precio;

        public Producto(int codigo, String descripcion, double precio)
        {
            _codigo = codigo;
            _descripcion = descripcion;
            _precio = precio;
        }

        public int Codigo
        {
            get { return _codigo; }
            set { _codigo = value; }
        }

        public String Descripcion
        {
            get { return _descripcion; }
            set { _descripcion = value; }
        }

        public double Precio
        {
            get { return _precio; }
            set { _precio = value; }
        }
    }
}

