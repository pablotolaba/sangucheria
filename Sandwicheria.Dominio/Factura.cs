﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Sandwicheria.Dominio
{
    public class Factura
    {
        public int _Concepto { get; set; }
        public int _DocTipo { get; set; }
        public long _DocNro { get; set; }
        public long _CbteDesde { get; set; }
        public long _CbteHasta { get; set; }
        public String _CbteFch { get; set; }// = $"{DateTime.Now:yyyyMMdd}";
        public double _ImpTotal { get; set; }
        public double _ImpTotConc { get; set; }
        public double _ImpNeto { get; set; }
        public double _ImpOpEx { get; set; }
        public double _ImpTrib { get; set; }
        public double _ImpIVA { get; set; }
        public String _FchServDesde { get; set; }
        public String _FchServHasta { get; set; }
        public String _FchVtoPago { get; set; }
        public String _MonId { get; set; }
        public double _MonCotiz { get; set; }
        public short _TributoId { get; set; }
        public String _TributoDesc { get; set; }
        public double _TributoBaseImp { get; set; }
        public double _TributoAlic { get; set; }
        public double _TributoImporte { get; set; }
        public int _AlicIvaId { get; set; }
        public double _AlicIvaBaseImp { get; set; }
        public double _AlicIvaImporte { get; set; }

    public Cabecera Cabecera { get; set; }
        public List<Detalle> Detalles { get; set; }
    }

    public class Cabecera
    {
        public int CantReg { get; set; }
        public int CbteTipo { get; set; }
        public int PtoVta { get; set; }
    }
    public class Detalle { }
}
