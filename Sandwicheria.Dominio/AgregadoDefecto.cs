﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sandwicheria.Dominio
{
    public class AgregadoDefecto
    {
        private String _descripcion { get; set; }

        public AgregadoDefecto(String descripcion)
        {
            _descripcion = descripcion;
        }
    }
}
