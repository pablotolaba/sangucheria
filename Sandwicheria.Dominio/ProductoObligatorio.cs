﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sandwicheria.Dominio
{
    public class ProductoObligatorio : Producto
    {
        public Tipo _tipo { get; set; }
        public List<AgregadoDefecto> _listaDefecto = new List<AgregadoDefecto>();
        public List<AgregadoOpcional> _listaOpcional = new List<AgregadoOpcional>();
        public ProductoObligatorio(int codigo, string descripcion, double precio, Tipo tipo) : base(codigo, descripcion, precio)
        {
            _tipo = tipo;
        }

        public double calcularTotalProducto()
        {
            double suma = 0;
            for (int i = 0; i< _listaOpcional.Count(); i++)
            {
                suma =+ _listaOpcional[i].Precio;
            }
            return base.Precio + suma;
        }

        public void agregarOpcional(AgregadoOpcional agregadoOpcional)
        {
            _listaOpcional.Add(agregadoOpcional);
        }

        public void agregarDefecto(AgregadoDefecto agregadoDefecto)
        {
            _listaDefecto.Add(agregadoDefecto);
        }
    }
}
