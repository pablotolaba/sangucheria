﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sandwicheria.Dominio
{
    public class Usuario
    {
        public String _contraseña { get; set; }
        public String _usuario { get; set; }

        public Usuario(String usuario, String contraseña)
        {
            _usuario = usuario;
            _contraseña = contraseña;

        }

    }
}
