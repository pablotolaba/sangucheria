﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sandwicheria.Dominio
{
    public class Pedido
    {
        private int _id = 0; 
        public DateTime _thisDay { get; set; }
        public double _total { get; set; }
        public Estado _estado { get; set; }
        public List<LineaDePedido> _listalineaPedido = new List<LineaDePedido>();

        public Pedido()
        {
            _id = ActualizarNumeroPedido();
            _thisDay = DateTime.Today;
            _estado = Estado.Pendiente;

        }

        public void AgregarLineaPedido(LineaDePedido lineaPedido)
        {
            _listalineaPedido.Add(lineaPedido);
        }

        public void CalcularTotal()
        {
            _total = 0;
            foreach (var lineaPedido in _listalineaPedido)
            {
                _total =+ lineaPedido.Subtotal;
            }
            
        }
        public int ActualizarNumeroPedido()
        {
            return _id++;
        }
        
    }
}
