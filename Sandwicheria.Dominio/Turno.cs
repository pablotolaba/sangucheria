﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sandwicheria.Dominio
{
    public class Turno
    {
        public Cajero _cajero { get; set; }
        public DateTime _fechaInicio { get; set; }
        public DateTime _fechaFin{ get; set; }
        public EstadoTurno _estadoTurno { get; set; }
        public double _rendicion { get; set; }
        public int _cantidadPedidos { get; set; }

        public Turno(Cajero cajero, DateTime fechaInicio)
        {
            _cajero = cajero;
            _fechaInicio = fechaInicio;
            _estadoTurno = EstadoTurno.Activo;
            
        }

        public void FinalizarTurno (DateTime fechaFin)
        {
            _fechaFin = fechaFin;
            _estadoTurno = EstadoTurno.Finalizado;
            CalcularRendicion();
        }
        
        public void CalcularRendicion()
        {
            _rendicion = 0;
            _cantidadPedidos = 0;
            foreach (var pedido in _cajero.Pedidos)
            {
                _rendicion += pedido._total;
                _cantidadPedidos++;
            }
          
        }
    }
}
