﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sandwicheria.Dominio
{
    public class LineaDePedido
    {
        private int _cantidad;
        private double _subtotal;
        public ProductoObligatorio _producto;

        public LineaDePedido(int cantidad,ProductoObligatorio producto)
        {
            _cantidad = cantidad;
            _producto = producto;
        }

        public int Cantidad
        {
            get { return _cantidad; }
            set { _cantidad = value; }
        }

        public double Subtotal
        {
            get { return _subtotal; }
            set { _subtotal = value; }
        }

        public ProductoObligatorio Producto
        {
            get { return _producto; }
            set { _producto = value; }
        }

        public void getSubtotal()
        {
            _subtotal = _cantidad * _producto.Precio;
        }
    }
}
