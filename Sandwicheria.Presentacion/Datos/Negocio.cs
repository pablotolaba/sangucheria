﻿using Sandwicheria.Dominio;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sandwicheria.Presentacion.Datos
{
    public class Negocio
    {

        private static List<Cajero> _cajeros = new List<Cajero>();
        public static List<Cajero> Productos => _cajeros;

        public static void Iniciar()
        {
            Cajero cajero1 = new Cajero(new Usuario("pepe2", "123456"));
            cajero1._codigo = 1;
            cajero1._nombre = "Jose Luis";
            _cajeros.Add(cajero1);
        }
        public static Cajero BuscarCajero(String usuario, String contraseña)
        {
            return _cajeros.FirstOrDefault(c => (c._usuario._usuario == usuario)&&(c._usuario._contraseña == contraseña));
        }
        public static bool EncontrarCajero(String usuario, String contraseña)
        {
           var existe = _cajeros.Any(c => (c._usuario._usuario == usuario) && (c._usuario._contraseña == contraseña));
            if (existe)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
