﻿using Sandwicheria.Dominio;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sandwicheria.Presentacion.Datos
{
   public class Almacen
    {
        private static List<ProductoObligatorio> _productos = new List<ProductoObligatorio>();
        public static List<ProductoObligatorio> Productos => _productos;
        private static List<AgregadoDefecto> _agregadosDefectos = new List<AgregadoDefecto>();
        public static List<AgregadoDefecto> AgregadosDefectos => _agregadosDefectos;
        public Almacen()
        {
            Iniciar();
        }
        public static void Iniciar()
        {
            _productos.Add(new ProductoObligatorio(1,"milanesa",65,Tipo.Sanguche));
            _productos.Add(new ProductoObligatorio(2,"hamburguesa",80,Tipo.Sanguche));
            _productos.Add(new ProductoObligatorio(11,"fanta",50,Tipo.Bebida));
            _productos.Add(new ProductoObligatorio(21,"papas",55,Tipo.Guarnicion));
        }
        public static Producto BuscarProducto(int codigo)
        {
            return _productos.FirstOrDefault(p => p.Codigo == codigo);
        }
       public static List<ProductoObligatorio> TraerProductoObligatorios(Tipo tipo)
        {
            var productos = new List<ProductoObligatorio>();
            foreach (var _prod in _productos)
            {
                if (_prod._tipo == tipo)
                {
                    productos.Add(_prod);
                }
            }
            return productos;
        }
    }
}
