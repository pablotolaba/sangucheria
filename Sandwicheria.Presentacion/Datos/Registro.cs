﻿using Sandwicheria.Dominio;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sandwicheria.Presentacion.Datos
{
    public class Registro
    {
        private static List<Turno> _listaTurnos = new List<Turno>();
        public static List<Turno> ListaTurnos => _listaTurnos;

        public static void AgregarTurno(Turno turno)
        {
            _listaTurnos.Add(turno);
        }


    }
}
