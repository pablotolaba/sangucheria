﻿using Sandwicheria.Dominio;
using Sandwicheria.Presentacion.Datos;
using Sandwicheria.Presentacion.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sandwicheria.Presentacion.Presentadores
{
    public class PresentadorPedidos
    {
        private IPedido _vistaPedido;
        private Pedido _pedido;
       // private Cajero _cajero;
        public PresentadorPedidos(IPedido vistaPedido, Cajero cajero)
        {
            _vistaPedido = vistaPedido;
            Almacen.Iniciar();
            _vistaPedido.ListarProductos(Almacen.Productos);
            _pedido = new Pedido();
        }
        public void CrearLineaPedido(LineaDePedido lineaPedido)
        {
            _pedido.AgregarLineaPedido(lineaPedido);
        }
        public Pedido GetPedido()
        {
            return _pedido;
        }
        public void DeterminarTipoProducto(ProductoObligatorio _producto)
        {
            if (_producto._tipo == Tipo.Sanguche)
            {
                _vistaPedido.MostrarAgregados(_producto);
            }
            else
            {
                CrearLineaPedido(_vistaPedido.LineaActual());
            }

        }
    }
}
