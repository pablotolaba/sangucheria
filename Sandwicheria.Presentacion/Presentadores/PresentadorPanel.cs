﻿using Sandwicheria.Dominio;
using Sandwicheria.Presentacion.Datos;
using Sandwicheria.Presentacion.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sandwicheria.Presentacion.Presentadores
{
    public class PresentadorPanel
    {
        private IPanel _vistaPanel;
        private Cajero _cajero;
        private Turno _turno;
        public PresentadorPanel(IPanel vistaPanel, Cajero cajero)
        {
            _vistaPanel = vistaPanel;
            _cajero = cajero;
            _turno = new Turno(_cajero, DateTime.Today);

        }
        public void FinalizarTurno()
        {
            _turno._estadoTurno = EstadoTurno.Finalizado;
            _turno.FinalizarTurno(DateTime.Today);
            Registro.AgregarTurno(_turno);
        }
    }
}
