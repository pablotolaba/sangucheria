﻿using Sandwicheria.Dominio;
using Sandwicheria.Presentacion.Datos;
using Sandwicheria.Presentacion.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sandwicheria.Presentacion.Presentadores
{
    public class PresentadorLogin
    {
        private ILogin _vistaLogin;
        public PresentadorLogin(ILogin vistaLogin)
        {
            _vistaLogin = vistaLogin;
            Negocio.Iniciar();
        }
        public bool IniciarSesion(String _usuario, String _contraseña)
        {

            return Negocio.EncontrarCajero(_usuario, _contraseña);

        }
        public Cajero RegregarCajero(String _usuario, String _contraseña)
        {
            return Negocio.BuscarCajero(_usuario, _contraseña);
        }
    }
}
