﻿using Sandwicheria.Dominio;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sandwicheria.Presentacion.Interfaces
{
    public interface IPedido
    {
        void ListarProductos(List<ProductoObligatorio> productos);
        void MostrarAgregados(ProductoObligatorio actual);
        LineaDePedido LineaActual();
    }
}
