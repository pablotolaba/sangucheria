﻿using Sandwicheria.Dominio;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sandwicheria.Presentacion.Interfaces
{
    public interface ILogin
    {
        void Autenticar(String _usuario, String _contraseña);
    }
}
