﻿using Sandwicheria.Dominio;
using Sandwicheria.Presentacion.Datos;
using Sandwicheria.Presentacion.Interfaces;
using Sandwicheria.Presentacion.Presentadores;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Sandwicheria.Presentacion.Vistas
{
    public partial class VistaPedido : Form // IPedido
    {
        private PresentadorPedidos _presentadorPedidos;
        private Cajero _cajero;  
        public VistaPedido(Cajero cajero)
        {
            InitializeComponent();
            _cajero = cajero;
       //     _presentadorPedidos = new PresentadorPedidos(this, _cajero);                   
        }
        public void ListarProductos(List<ProductoObligatorio> productos)
        {
            //ProductosBindingSource.DataSource = productos;
        }

        private ProductoObligatorio GetProducto()
        {
            //var actual = ProductosBindingSource.Current as ProductoObligatorio;
            //if (actual != null) return actual;
            MessageBox.Show("Debe seleccionar un producto", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
            return null;
        }

        private void btnAceptarProducto_Click(object sender, EventArgs e)
        {
            _presentadorPedidos.DeterminarTipoProducto(GetProducto());
            //PedidoBindingSource.DataSource = _presentadorPedidos.GetPedido()._listalineaPedido;
        }
        public void MostrarAgregados(ProductoObligatorio actual)
        {
            var agregados = new VistaArmado(actual);
            agregados.ShowDialog();
        }
        //public LineaDePedido LineaActual()
        //{
         //   var actual = GetProducto();
           // var cantidad = int.Parse(tbCantidad.Text);
            //return new LineaDePedido(cantidad, actual);
        //}

      
    }
}
