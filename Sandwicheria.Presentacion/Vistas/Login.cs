﻿using Sandwicheria.Dominio;
using Sandwicheria.Presentacion.Datos;
using Sandwicheria.Presentacion.Interfaces;
using Sandwicheria.Presentacion.Presentadores;
using Sandwicheria.Presentacion.Vistas;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Sandwicheria.Presentacion
{
    public partial class Login : Form, ILogin
    {
        private PresentadorLogin _presentadorLogin;
        public Login()
        {
            InitializeComponent();           

            _presentadorLogin = new PresentadorLogin(this);         
        }
        private void btnCancelar_Click(object sender, EventArgs e)
        {
            Close();
        }
        private void btbIniciar_Click(object sender, EventArgs e)
        {
            this.Hide();
            Autenticar(txbUsuario.Text, txbContrasenia.Text);
            this.Close();           
        }

        public void Autenticar(String _usuario, String _contraseña)
        {
            if (!_presentadorLogin.IniciarSesion(_usuario,_contraseña))
            {
                MessageBox.Show("Error en el Inicio de Sesion");
            }
            else
            {
                var cajero = _presentadorLogin.RegregarCajero(_usuario, _contraseña);
                MessageBox.Show("Bienvenido al Sistema: "+ cajero._nombre);
                new VistaPanel(cajero).ShowDialog();
            }
        }
        
    }
}
