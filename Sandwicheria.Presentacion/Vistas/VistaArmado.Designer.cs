﻿namespace Sandwicheria.Presentacion.Vistas
{
    partial class VistaArmado
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cbHuevo = new System.Windows.Forms.CheckBox();
            this.cbQueso = new System.Windows.Forms.CheckBox();
            this.cbJamon = new System.Windows.Forms.CheckBox();
            this.cbSabora = new System.Windows.Forms.CheckBox();
            this.cbMayonesa = new System.Windows.Forms.CheckBox();
            this.cbLechuga = new System.Windows.Forms.CheckBox();
            this.cbTomate = new System.Windows.Forms.CheckBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btbAceptar = new System.Windows.Forms.Button();
            this.lblNumSanguche = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // cbHuevo
            // 
            this.cbHuevo.AutoSize = true;
            this.cbHuevo.Location = new System.Drawing.Point(243, 145);
            this.cbHuevo.Margin = new System.Windows.Forms.Padding(2);
            this.cbHuevo.Name = "cbHuevo";
            this.cbHuevo.Size = new System.Drawing.Size(58, 17);
            this.cbHuevo.TabIndex = 19;
            this.cbHuevo.Text = "Huevo";
            this.cbHuevo.UseVisualStyleBackColor = true;
            // 
            // cbQueso
            // 
            this.cbQueso.AutoSize = true;
            this.cbQueso.Location = new System.Drawing.Point(244, 113);
            this.cbQueso.Margin = new System.Windows.Forms.Padding(2);
            this.cbQueso.Name = "cbQueso";
            this.cbQueso.Size = new System.Drawing.Size(57, 17);
            this.cbQueso.TabIndex = 18;
            this.cbQueso.Text = "Queso";
            this.cbQueso.UseVisualStyleBackColor = true;
            // 
            // cbJamon
            // 
            this.cbJamon.AutoSize = true;
            this.cbJamon.Location = new System.Drawing.Point(244, 79);
            this.cbJamon.Margin = new System.Windows.Forms.Padding(2);
            this.cbJamon.Name = "cbJamon";
            this.cbJamon.Size = new System.Drawing.Size(57, 17);
            this.cbJamon.TabIndex = 17;
            this.cbJamon.Text = "Jamon";
            this.cbJamon.UseVisualStyleBackColor = true;
            // 
            // cbSabora
            // 
            this.cbSabora.AutoSize = true;
            this.cbSabora.Location = new System.Drawing.Point(101, 156);
            this.cbSabora.Margin = new System.Windows.Forms.Padding(2);
            this.cbSabora.Name = "cbSabora";
            this.cbSabora.Size = new System.Drawing.Size(60, 17);
            this.cbSabora.TabIndex = 16;
            this.cbSabora.Text = "Sabora";
            this.cbSabora.UseVisualStyleBackColor = true;
            // 
            // cbMayonesa
            // 
            this.cbMayonesa.AutoSize = true;
            this.cbMayonesa.Location = new System.Drawing.Point(101, 125);
            this.cbMayonesa.Margin = new System.Windows.Forms.Padding(2);
            this.cbMayonesa.Name = "cbMayonesa";
            this.cbMayonesa.Size = new System.Drawing.Size(75, 17);
            this.cbMayonesa.TabIndex = 15;
            this.cbMayonesa.Text = "Mayonesa";
            this.cbMayonesa.UseVisualStyleBackColor = true;
            // 
            // cbLechuga
            // 
            this.cbLechuga.AutoSize = true;
            this.cbLechuga.Location = new System.Drawing.Point(101, 92);
            this.cbLechuga.Margin = new System.Windows.Forms.Padding(2);
            this.cbLechuga.Name = "cbLechuga";
            this.cbLechuga.Size = new System.Drawing.Size(68, 17);
            this.cbLechuga.TabIndex = 14;
            this.cbLechuga.Text = "Lechuga";
            this.cbLechuga.UseVisualStyleBackColor = true;
            // 
            // cbTomate
            // 
            this.cbTomate.AutoSize = true;
            this.cbTomate.Location = new System.Drawing.Point(101, 55);
            this.cbTomate.Margin = new System.Windows.Forms.Padding(2);
            this.cbTomate.Name = "cbTomate";
            this.cbTomate.Size = new System.Drawing.Size(62, 17);
            this.cbTomate.TabIndex = 13;
            this.cbTomate.Text = "Tomate";
            this.cbTomate.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(70, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(212, 24);
            this.label1.TabIndex = 20;
            this.label1.Text = "Armado Sanguche N°";
            // 
            // btbAceptar
            // 
            this.btbAceptar.Location = new System.Drawing.Point(163, 207);
            this.btbAceptar.Name = "btbAceptar";
            this.btbAceptar.Size = new System.Drawing.Size(75, 23);
            this.btbAceptar.TabIndex = 21;
            this.btbAceptar.Text = "Aceptar";
            this.btbAceptar.UseVisualStyleBackColor = true;
            // 
            // lblNumSanguche
            // 
            this.lblNumSanguche.AutoSize = true;
            this.lblNumSanguche.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNumSanguche.Location = new System.Drawing.Point(288, 9);
            this.lblNumSanguche.Name = "lblNumSanguche";
            this.lblNumSanguche.Size = new System.Drawing.Size(32, 24);
            this.lblNumSanguche.TabIndex = 22;
            this.lblNumSanguche.Text = "00";
            // 
            // VistaArmado
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(404, 254);
            this.Controls.Add(this.lblNumSanguche);
            this.Controls.Add(this.btbAceptar);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cbHuevo);
            this.Controls.Add(this.cbQueso);
            this.Controls.Add(this.cbJamon);
            this.Controls.Add(this.cbSabora);
            this.Controls.Add(this.cbMayonesa);
            this.Controls.Add(this.cbLechuga);
            this.Controls.Add(this.cbTomate);
            this.Name = "VistaArmado";
            this.Text = "VistaArmado";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.CheckBox cbHuevo;
        private System.Windows.Forms.CheckBox cbQueso;
        private System.Windows.Forms.CheckBox cbJamon;
        private System.Windows.Forms.CheckBox cbSabora;
        private System.Windows.Forms.CheckBox cbMayonesa;
        private System.Windows.Forms.CheckBox cbLechuga;
        private System.Windows.Forms.CheckBox cbTomate;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btbAceptar;
        private System.Windows.Forms.Label lblNumSanguche;
    }
}