﻿namespace Sandwicheria.Presentacion.Vistas
{
    partial class VistaPanel
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnNuevoPedido = new System.Windows.Forms.Button();
            this.btnFinalizarTurno = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.lblNumEmpleado = new System.Windows.Forms.Label();
            this.lblNombreEmpleado = new System.Windows.Forms.Label();
            this.lblNombre = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnNuevoPedido
            // 
            this.btnNuevoPedido.Location = new System.Drawing.Point(183, 164);
            this.btnNuevoPedido.Name = "btnNuevoPedido";
            this.btnNuevoPedido.Size = new System.Drawing.Size(122, 41);
            this.btnNuevoPedido.TabIndex = 0;
            this.btnNuevoPedido.Text = "Realizar Pedido";
            this.btnNuevoPedido.UseVisualStyleBackColor = true;
            this.btnNuevoPedido.Click += new System.EventHandler(this.btnNuevoPedido_Click);
            // 
            // btnFinalizarTurno
            // 
            this.btnFinalizarTurno.Location = new System.Drawing.Point(183, 246);
            this.btnFinalizarTurno.Name = "btnFinalizarTurno";
            this.btnFinalizarTurno.Size = new System.Drawing.Size(122, 41);
            this.btnFinalizarTurno.TabIndex = 1;
            this.btnFinalizarTurno.Text = "Finalizar Turno";
            this.btnFinalizarTurno.UseVisualStyleBackColor = true;
            this.btnFinalizarTurno.Click += new System.EventHandler(this.btnFinalizarTurno_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(153, 31);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(145, 24);
            this.label1.TabIndex = 2;
            this.label1.Text = "Empleado N°: ";
            // 
            // lblNumEmpleado
            // 
            this.lblNumEmpleado.AutoSize = true;
            this.lblNumEmpleado.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNumEmpleado.Location = new System.Drawing.Point(304, 31);
            this.lblNumEmpleado.Name = "lblNumEmpleado";
            this.lblNumEmpleado.Size = new System.Drawing.Size(32, 24);
            this.lblNumEmpleado.TabIndex = 3;
            this.lblNumEmpleado.Text = "00";
            // 
            // lblNombreEmpleado
            // 
            this.lblNombreEmpleado.AutoSize = true;
            this.lblNombreEmpleado.Location = new System.Drawing.Point(12, 31);
            this.lblNombreEmpleado.Name = "lblNombreEmpleado";
            this.lblNombreEmpleado.Size = new System.Drawing.Size(0, 13);
            this.lblNombreEmpleado.TabIndex = 4;
            // 
            // lblNombre
            // 
            this.lblNombre.AutoSize = true;
            this.lblNombre.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNombre.Location = new System.Drawing.Point(179, 68);
            this.lblNombre.Name = "lblNombre";
            this.lblNombre.Size = new System.Drawing.Size(82, 24);
            this.lblNombre.TabIndex = 5;
            this.lblNombre.Text = "nombre";
            // 
            // VistaPanel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(485, 349);
            this.Controls.Add(this.lblNombre);
            this.Controls.Add(this.lblNombreEmpleado);
            this.Controls.Add(this.lblNumEmpleado);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnFinalizarTurno);
            this.Controls.Add(this.btnNuevoPedido);
            this.Name = "VistaPanel";
            this.Text = "PanelEmpleado";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnNuevoPedido;
        private System.Windows.Forms.Button btnFinalizarTurno;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblNumEmpleado;
        private System.Windows.Forms.Label lblNombreEmpleado;
        private System.Windows.Forms.Label lblNombre;
    }
}