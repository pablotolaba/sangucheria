﻿using Sandwicheria.Dominio;
using Sandwicheria.Presentacion.Datos;
using Sandwicheria.Presentacion.Interfaces;
using Sandwicheria.Presentacion.Presentadores;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Sandwicheria.Presentacion.Vistas
{
    public partial class VistaPanel : Form, IPanel
    {
        private PresentadorPanel _presentador;
        private Cajero _cajero;
        private Turno _turno;
        public VistaPanel(Cajero cajero)
        {
            InitializeComponent();
            _cajero = cajero;
            _presentador = new PresentadorPanel(this,cajero);



            MostrarCajero();
        }

        private void btnNuevoPedido_Click(object sender, EventArgs e)
        {
            var nuevoPedido = new VistaPedido(_cajero);
            nuevoPedido.ShowDialog();
        }
        public void MostrarCajero()
        {
            lblNumEmpleado.Text = _cajero._codigo.ToString();
            lblNombre.Text = _cajero._nombre;
        }

        private void btnFinalizarTurno_Click(object sender, EventArgs e)
        {
            _presentador.FinalizarTurno();
        }
    }
}
