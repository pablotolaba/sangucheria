﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sandwicheria.DatosExternos.LoginService;
using Sandwicheria.DatosExternos.ReferenciaAFIP;
using Sandwicheria.Dominio;

namespace Sandwicheria.DatosExternos
{
    public class ServicioExternoAfip
    {
        private Factura f;
        private string codigoGrupo = "22D7E8D-C5C1-4FFE-9CBD-E517A889DB3F";
        private Autorizacion _autorizacion;
        public FECAESolicitarRequest _ultimoAutorizado;
        public DateTime _fecha;

        public ServicioExternoAfip(Factura _f)
        {
            f = _f;
        }

        public void LoginAfip()
        {
            var cliente = new LoginServiceClient();

            _autorizacion = cliente.SolicitarAutorizacion(codigoGrupo);
        }
        public void SolicitarUltimoCupon(Factura factura)
        {
            
            var auth = new FEAuthRequest
            {
                Cuit = _autorizacion.Cuit,
                Sign = _autorizacion.Sign,
                Token = _autorizacion.Token
            };
            var fecaeRequest = new FECAERequest
            {
                FeCabReq = new FECAECabRequest
                {
                    CantReg = factura.Cabecera.CantReg,
                    CbteTipo = factura.Cabecera.CbteTipo,
                    PtoVta = factura.Cabecera.PtoVta
                },
                FeDetReq = _detalles(factura.Detalles)
            };
            var fecaeSoliciarRequestBody = new FECAESolicitarRequestBody();
            var fecaeSolicitar = new FECAESolicitarRequest(new FECAESolicitarRequestBody());

            var _solicitarAutorizazion = new ServiceSoapClient();
            _solicitarAutorizazion.FECAESolicitar(auth, fecaeRequest);
        }

        private FECAEDetRequest[] _detalles(List<Detalle> detalles)
        {
            var items = new List<FECAEDetRequest>();
            foreach (var item in detalles)
            {

                items.Add(new FECAEDetRequest
                {
                    Concepto = f._Concepto,
                    DocTipo = f._DocTipo,
                    DocNro = f._DocNro,
                    CbteDesde = f._CbteDesde,
                    CbteHasta = f._CbteHasta,
                    CbteFch = f._CbteFch,
                    ImpTotal = f._ImpTotal,
                    ImpTotConc = f._ImpTotConc,
                    ImpNeto = f._ImpNeto,
                    ImpOpEx = f._ImpOpEx,
                    ImpTrib = f._ImpTrib,
                    ImpIVA = f._ImpIVA,
                    FchServDesde = f._FchServDesde,
                    FchServHasta = f._FchServHasta,
                    FchVtoPago = f._FchVtoPago,
                    MonId = f._MonId,
                    MonCotiz = f._MonCotiz,
                    /* Tributos = new Tributo[]
                    {
                        Id = f._TributoId,
                        Desc = f._TributoDesc,
                        BaseImp = f._TributoBaseImp,
                        Alic = f._TributoAlic,
                        Importe = f._TributoImporte
                    },
                    Iva = new AlicIva()
                    {
                        Id = f._AlicIvaId,
                        BaseImp = f._AlicIvaBaseImp,
                        Importe = f._AlicIvaImporte
                    }*/
                });
            }

            return items.ToArray();
        }
    }
}
