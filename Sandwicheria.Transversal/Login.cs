﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sandwicheria.Transversal
{
    public class Login
    {
        public DateTime _fechaInicioSesion { get; set; }
        public DateTime _fechaFinSesion { get; set; }

        public Login(DateTime fechaInicioSesion)
        {
            _fechaInicioSesion = fechaInicioSesion;
        }

        public void FinalizarLogin(DateTime fechaFinSesion)
        {
            _fechaFinSesion = fechaFinSesion;
        }

    }
}
